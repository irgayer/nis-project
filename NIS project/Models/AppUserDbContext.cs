﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NIS_project.Models
{
    public class AppUserDbContext : IdentityDbContext<User>
    {
        public AppUserDbContext(DbContextOptions<AppUserDbContext> options) : base(options)
        { 
            //Database.EnsureDeleted();
            Database.EnsureCreated();
            //  Database.Migrate();
        }
    }
}
