﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NIS_project.Models
{
    public interface ISubjectRepository//Тоже самое почти, только есть добавить материал(IUserRepository)
    { 
        List<Subject> Subjects { get; }
        void AddSubject(Subject subject);
        void AddMaterial(int id, Material material);
    }
}
