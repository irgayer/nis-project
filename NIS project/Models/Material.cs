﻿namespace NIS_project.Models
{
    public class Material
    {
        public int Id { get; set; }//Для БД
        public string Name { get; set; }//Имя материала
        public string Path { get; set; } //Путь до файла на сервере
    }
}