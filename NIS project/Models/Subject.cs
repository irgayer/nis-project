﻿ using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace NIS_project.Models
{
    public class Subject //Класс предмета, Id - для БД, Имя и список материалов
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Material> Materials { get; set; }
    }
}