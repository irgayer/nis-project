﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NIS_project.Models
{
    public class User : IdentityUser
    {
        [Required]
        public string Password { get; set; }
        public bool IsAdmin { get; set; }
    }
}
