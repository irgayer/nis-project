﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace NIS_project.Models
{
    public class EFSubjectRepository : ISubjectRepository //Настоящая репозитория
    {
        AppDbContext context;

        public EFSubjectRepository(AppDbContext context)//БД
        {
            this.context = context;
        }

        public List<Subject> Subjects => context.Subjects.ToList();
        public void AddSubject(Subject subject)
        {
            context.Subjects.Add(subject); //Добавление в бд предмета и сохранение изменений
            context.SaveChanges();
        }

        public void AddMaterial(int id, Material material) //Добавить материал в предмет
        {
            var subject = context.Subjects.FirstOrDefault(s => s.Id == id);

            if (subject != null)//Если такой предмет есть
            {
                if (subject.Materials == null)
                    subject.Materials = new List<Material>();

                subject.Materials.Add(material);//Добавление
                context.SaveChanges();//Сохранение изменений
            }
        }
    }
}
