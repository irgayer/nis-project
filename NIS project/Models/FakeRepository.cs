﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace NIS_project.Models
{
    public class FakeRepository : ISubjectRepository, IUserRepository //Фейковая БД, для тестов
    {
        public List<Subject> Subjects
        {
            get;
            set;
        } = new List<Subject>();

        public FakeRepository()
        {
            DataSeed();
        }

        private void DataSeed()
        {
            Subjects.AddRange(new List<Subject>
                {
                    new Subject {
                        Name = "Biology",
                        Id = 1,
                        Materials = new List<Material>
                        {
                            new Material { Name = "Mitosis"},
                            new Material { Name = "Delenie kletki"},
                            new Material { Name = "Milost allaha"}
                        }},
                    new Subject {
                        Name = "Physics",
                        Id = 2,
                        Materials = new List<Material>
                        {
                            new Material { Name = "Qvant mechanics"},
                            new Material { Name = "Power Ahiles"},
                            new Material { Name = "Qvant electrons" }
                        }}
                });
            //Users.Add(new User { IsAdmin = true, Login = "admin", Password = "123" });

        }

    public List<User> Users { get; } = new List<User>();

    public void AddSubject(Subject subject)
    {

    }

    public void AddMaterial(int id, Material material)
    {
        throw new NotImplementedException();
    }

    public void AddUser(User user)
    {
        Users.Add(user);
    }

    public static List<Subject> Fake()
    {
        return
            new List<Subject>
            {
                    new Subject {Name = "Biology"},
                    new Subject {Name = "Physics"},
                    new Subject {Name = "Chemistry"},
                    new Subject {Name = "History"}
            };
    }
}
}
