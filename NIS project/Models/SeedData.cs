﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace NIS_project.Models
{
    public static class SeedData //Чисто для тестов, чтобы не был пустой список предметов
    //Тут добавление предметов Biology, Milost Allaha и тд
    {
        public static void EnsurePopulated(IApplicationBuilder app)
        {
            AppDbContext context = app.ApplicationServices.GetRequiredService<AppDbContext>();
                //context.Database.Migrate();

            if (!context.Subjects.Any())
            {
                context.Subjects.AddRange(new List<Subject>
                {
                    new Subject {
                        Name = "Biology"
                    },
                    new Subject {
                        Name = "Physics",
                    },
                    new Subject
                    {
                        Name = "Milost Allaha"
                    }
                });
                //context.SaveChanges();
                //context.Subjects.ToList().FirstOrDefault(s => s.Name == "Biology")
                //    ?.Materials.AddRange(new List<Material>
                //    {
                //        new Material { Name = "Qvant mechanics"},
                //        new Material { Name = "Power Ahiles"},
                //        new Material { Name = "Qvant electrons" }
                //    });
                //context.Subjects.ToList().FirstOrDefault(s => s.Name == "Biology")
                //    ?.Materials.AddRange(new List<Material>
                //{
                //    new Material { Name = "Mitosis"},
                //    new Material { Name = "Delenie kletki"},
                //    new Material { Name = "Milost allaha"}
                //});
                context.SaveChanges();
            }
        }
    }
}
