﻿using Microsoft.AspNetCore.Identity;

using System.Threading.Tasks;

namespace NIS_project.Models
{
    public static class IdentitySeedData//Чисто добавление админа. Я не использовала
    {
        const string ADMIN_LOGIN = "admin";
        const string ADMIN_PASSWORD = "admin123";

        public static async Task EnsurePopulated(UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
        {
            if (await roleManager.FindByNameAsync("admin") == null)
                await roleManager.CreateAsync(new IdentityRole("admin"));

            var admin = await userManager.FindByNameAsync(ADMIN_LOGIN);

            if (admin == null)
            {
                admin = new User { UserName = ADMIN_LOGIN, IsAdmin = true, Password = ADMIN_PASSWORD};
                await userManager.CreateAsync(admin, ADMIN_PASSWORD);
                await userManager.AddToRoleAsync(admin, "admin");
            }
        }
    }
}
