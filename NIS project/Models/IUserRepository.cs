﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NIS_project.Models
{
    public interface IUserRepository//Интерфейс. Список пользователей и добавление юзеров
    {
        List<User> Users { get; }
        void AddUser(User user);
    }
}
