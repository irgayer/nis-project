﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NIS_project.Migrations
{
    public partial class Sec : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Material_Subjects_Id",
                table: "Material");

            migrationBuilder.DropIndex(
                name: "IX_Material_Id",
                table: "Material");

            migrationBuilder.AddColumn<string>(
                name: "Path",
                table: "Material",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SubjectId",
                table: "Material",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Material_SubjectId",
                table: "Material",
                column: "SubjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_Material_Subjects_SubjectId",
                table: "Material",
                column: "SubjectId",
                principalTable: "Subjects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Material_Subjects_SubjectId",
                table: "Material");

            migrationBuilder.DropIndex(
                name: "IX_Material_SubjectId",
                table: "Material");

            migrationBuilder.DropColumn(
                name: "Path",
                table: "Material");

            migrationBuilder.DropColumn(
                name: "SubjectId",
                table: "Material");

            migrationBuilder.CreateIndex(
                name: "IX_Material_Id",
                table: "Material",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Material_Subjects_Id",
                table: "Material",
                column: "Id",
                principalTable: "Subjects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
