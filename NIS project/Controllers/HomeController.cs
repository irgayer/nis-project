﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NIS_project.Models;

namespace NIS_project.Controllers
{
    public class HomeController : Controller
    {
        private ISubjectRepository subjectRepository; //Репозиторий предметов

        public HomeController(ISubjectRepository repo) //Инициализация репозитория с помощью ASP .NET
        {
            subjectRepository = repo;
        }

        public IActionResult Faq() //Вкладка Faq.cshtml
        {
            return View();
        }

        [Authorize] //Если юзер залогинен, то показать страницу Index.cshtml со списком предметов из репозитория
        //Если нет, показать страницу логина 
        public ActionResult Index()
        {
            return View(subjectRepository.Subjects);
        }
    }
}