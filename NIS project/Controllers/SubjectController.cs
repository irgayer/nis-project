﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Schema;
using NIS_project.Models;
using NIS_project.ViewModels;

namespace NIS_project.Controllers
{
    public class SubjectController : Controller
    {
        private ISubjectRepository repository; //Список предметов
        IWebHostEnvironment appEnvironment; //хз

        public SubjectController(ISubjectRepository repo, IWebHostEnvironment app) //Инициализация
        {
            repository = repo;
            appEnvironment = app;
        }

        public IActionResult Index() //Если юзер хочет войти на Index.cshtml папки Views/Subjects, то его перебрасывает на дом. страницу
        {
            return RedirectToAction("Index", "Home");
        }

        public IActionResult Subjects(int id) //Показать материалы предмета с id из базы данных предметов  
        {
            var subject = repository.Subjects.FirstOrDefault(s => s.Id == id);
            var materials = subject.Materials;
            ViewBag.SubjectName = subject.Name;
            return View(materials);
        }

        [HttpGet("download")]
        public async Task<IActionResult> DownloadMaterial(string fileName) //Скачать материал, приходит имя материала
        {
            var path = Path.Combine( //Инициализируется путь файла материала
                Directory.GetCurrentDirectory(),
                "wwwroot\\files", fileName);

            var memory = new MemoryStream(); //Инициализация памяти
            try
            {
                using (var stream = new FileStream(path, FileMode.Open)) //Инициализация потока I/O
                {
                    await stream.CopyToAsync(memory); //Копирование в файл
                }

                memory.Position = 0; //Позиция начала файла с 0, то есть с начала файла до конца
                return File(memory, GetContentType(path), Path.GetFileName(path)); //Скачивание файла
            }
            catch (Exception exception) //Шобы не сломалось
            {
                // ignored
            }

            return RedirectToAction("Index", "Home"); //Перебросить домой
        }

        private string GetContentType(string path) //Узнать какой тип файла приходит
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }

        private Dictionary<string, string> GetMimeTypes() //Допустимые типы файлов
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet"},
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"},
                {".ppt", "presentation"},
                {".pptm", "presentation"},
                {".pptx", "presentation"}
            };
        }

        //[Authorize(Roles = "admin")]
        public IActionResult AddSubject() //Форма добавления предметов
        {
            var subjectNames = repository.Subjects.Select(s => s.Name);
            var subjectModel = new SubjectModel {ExistingSubjectNames = subjectNames.ToList()};
            return View(subjectModel);
        }

        [HttpPost]
        [Authorize(Roles = "admin")]//Если пользователь является админом, иначе запретить доступ
        public IActionResult AddSubject(SubjectModel model) //Приходит модель, в которой хранится имя нового предмета
        {
            repository.AddSubject(new Subject {Name = model.Name}); //Добавление предмета в БД

            return RedirectToAction("AddSubject", "Subject"); //Перебросить обратно на форму
        }

        public IActionResult AddMaterialToSubject(int id) //Форма добавление материала в предмет под индексом Id в БД
        {
            var str = repository.Subjects.FirstOrDefault(s => s.Id == id)?.Name; 
            if (str == null)// Если такой предмет не найден, перебросить домой
                return RedirectToAction("Index", "Home");

            ViewBag.SubjectName = str; //Передать в форму имя предмета
            ViewBag.SubjectId = id; //И айди предмета, БЕШАННЫЙ КОСТЫЛЬ НА*УЙ
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "admin")]//Если пользователь является админом, иначе запретить доступ
        public async Task<IActionResult> AddMaterialToSubject(FileModel model) //Приходит форма, в которой Id предмета
        //куда добавлять материалы и сами файлы материалов
        {
            var id = model.Id;
            long size = model.Files.Sum(f => f.Length);//Высчитывание длинны файлов
            var paths = new List<string>(); //Инициализация списка путей
            if (model.Files != null)//Если хоть один файл был выбран на загрузку
            {
                foreach (var file in model.Files)//Пробежать по каждому файлу
                {
                    if (file.Length > 0)//Если он не пустой
                    {
                        var path = "/files/" + file.FileName; //Инициализировать путь
                        paths.Add(path); //Добавить путь в список, нахуя? я не помню
                        try
                        {
                            //Инициализировать поток I/O
                            using (var fileStream = new FileStream(appEnvironment.WebRootPath + path, FileMode.Create))
                            {
                                //Скопировать
                                await file.CopyToAsync(fileStream);
                            }
                            //Добавить материал в предмет из репозитория 
                            repository.AddMaterial(id,
                                new Material {Path = appEnvironment.WebRootPath + path, Name = file.FileName});
                        }
                        catch (Exception exception)
                        {
                            Console.WriteLine(exception.Message);
                        }
                    }
                }

                //Тоже не помню
                
                //string path = "/files/" + model.Files.FileName;
                //string subjectId = Request.QueryString.Value;

                //using (var fileStream = new FileStream(appEnvironment.WebRootPath + path, FileMode.Create))
                //{
                //    await model.Files.CopyToAsync(fileStream);
                //}

                //repository.AddMaterial(id, new Material { Path = appEnvironment.WebRootPath + path, Name = model.Files.FileName });
            }

            return RedirectToAction("Index", "Home");//Перекинуть домой
        }

        //[Authorize(Roles = "admin")]
        public IActionResult AddMaterial()//Вернуть предметы
        {
            return View(repository.Subjects);
        }
    }
}