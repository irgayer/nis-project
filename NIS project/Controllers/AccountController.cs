﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Reflection.Metadata;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp;
using NIS_project.Models;
using NIS_project.ViewModels;

namespace NIS_project.Controllers
{
    public class AccountController : Controller
    {
        const int MIN_PASSWORD_LENGHT = 6;

        //IUserRepository repository;
        readonly SignInManager<User> signInManager;//Манагер для входа, аутентификации и добавления куки
        readonly UserManager<User> userManager;  //манагер для создания юзеров
        readonly RoleManager<IdentityRole> roleManager;//Манагер для определения админа 
        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager, RoleManager<IdentityRole> roleManager)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.roleManager = roleManager;

            //IdentitySeedData.EnsurePopulated(userManager, roleManager);
        }
        
        [HttpGet]
        public IActionResult Index()//Возвращение формы логина
        {
            return View("Login");
        }

        [HttpGet]
        public IActionResult Login()//Возвращение формы логина
        {
            return View();
        }
        [HttpGet]
        public IActionResult Register()//Возвращение формы регистрации
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]//Шобы юзер запоминался при переходе на другие страницы
        public async Task<IActionResult> Login(LoginModel model) //Приходит модель юзера, там пароль и логин
        {
            if (ModelState.IsValid)//Если в форме все норм
            {
                //Попытаться войти
                var result = await signInManager.PasswordSignInAsync(model.Login, model.Password, false, false);

                if (result.Succeeded)//Если окей, перебросить на домашнюю страницу
                {
                    return RedirectToAction("Index", "Home");
                }
                //var user = repository.Users.FirstOrDefault(u =>
                //    u.Login == model.Login && u.Password == model.Password);

                //if (user != null)
                //{
                //    //await Authenticate(model.Login);
                //    await signInManager.SignInAsync(user, true);
                //    return RedirectToAction("Index", "Home");
                //}
                //Иначе добавить ошибку
                ModelState.AddModelError("", "Uncorrect login or password");
            }

            return View();//Перезагрузить страницу
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterModel model)//Приходит модель, там логин, пароль
        {
            if (ModelState.IsValid)
            {
                //Если пароль длинной меньше 6 
                if (!(model.Password.Length >= MIN_PASSWORD_LENGHT))
                {
                    //Добавить ошибку и перезагрузить страницу
                    ModelState.AddModelError("", "Password must be longer than 6 characters");

                    return View();
                }

                //Создание юзера 
                var user = new User {UserName = model.Login ,IsAdmin = false, Password = model.Password };
                var result = await userManager.CreateAsync(user, user.Password);

                if (result.Succeeded)//Если все ок, автоматически войти и перебросить на домашнюю страницу
                {
                    await signInManager.SignInAsync(user, false);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    foreach (var error in result.Errors)//Иначе отобразить все ошибки
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }

                //var user = repository.Users.FirstOrDefault(u => u.Login == model.Login);

                //if (user == null)
                //{
                //    repository.AddUser(new User() {IsAdmin = false, Login = model.Login, Password = model.Password});
                //    await signInManager.SignInAsync(user, false);

                //    return View("TestUsers", repository.Users);
                //    return RedirectToAction("Index", "Home");
                //}
                //else
                //{
                //    ModelState.AddModelError("", "Login is already exists");
                //}

                //if (ModelState.IsValid)
                //{
                //    if (repository.Users.Any(x => x.Login == user.Login))
                //        return View();

                //    repository.Users.Add(user);
                //}
            }

            return View();//Перезагрузить страницу
        }

      

        private async Task Authenticate(string login)//Нахуя? я не помню
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, login)
            };

            var id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);

            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }

        public async Task<IActionResult> SignOut()//Выход из акка и переход на страницу логина
        {
            await signInManager.SignOutAsync();

            return RedirectToAction("Login", "Account");
        }
    }
}
