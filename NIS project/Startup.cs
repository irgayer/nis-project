using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NIS_project.Models;

namespace NIS_project
{
    public class Startup
    {
        IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddDbContext<AppDbContext>();
            //services.AddSingleton<ISubjectRepository, FakeRepository>();
            //services.AddSingleton<IUserRepository, FakeRepository>();

            services.AddDbContext<AppUserDbContext>(options =>//Добавление БД Юзеров
            {
                options.UseSqlServer(Configuration.GetConnectionString("IdentityConnection"));
            });
            services.AddDbContext<AppDbContext>(options => //Добавление БД Предметов
            {
                options.UseSqlServer(Configuration.GetConnectionString("SubjectsConnection"));
            });
            services.AddIdentity<User, IdentityRole>(opts =>//Добавление Идентификаций пользователей
            {
                //Требования
                opts.Password.RequiredLength = 6; //Пароль больше 6 символов
                opts.Password.RequireDigit = false;//Нужны цифры
                opts.Password.RequireLowercase = false;//Нужны маленькие символы
                opts.Password.RequireUppercase = false;//Нужны большие символы
                opts.Password.RequireNonAlphanumeric = false;//Нужны символы типа ?, *, (, ", №, ;, :
                opts.Password.RequiredUniqueChars = 0; //Их количество
            }).AddEntityFrameworkStores<AppUserDbContext>();
            services.AddSingleton<ISubjectRepository, EFSubjectRepository>(); //Добавление репозитория, именно он инициализируется везде
            services.AddMemoryCache(); //Кэшмемори, саламалейкум википедия
            services.AddMvc(s => s.EnableEndpointRouting = false);//Добавление паттерна MVC
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme) //Добавление аутентификации по куки
                .AddCookie(options => { options.LoginPath = new PathString("/Account/Login"); });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            app.UseRouting();//Использование роутинга 
            app.UseDeveloperExceptionPage();//Использование страницы где отображаются ошибки, если приложение ломается
            app.UseStatusCodePages(); //добавление страниц 404,504 и тд, ошибки в общем
            app.UseStaticFiles();//Использование статических файлов для СSS
            app.UseAuthentication(); //Использование аутентификации
            app.UseAuthorization();//Использование авторизации
            app.UseHttpsRedirection(); //Использование безопасного редиректа


            app.UseMvc(routes =>//Использование паттерна MVC
            {
                routes.MapRoute //По умолчанию домашняя страница 
                (
                    name: "default",
                    template: "{controller=Home}/{action=Index}/"
                );
            });
                //SeedData.EnsurePopulated(app);
        }
    }
}
