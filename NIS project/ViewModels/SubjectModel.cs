﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NIS_project.ViewModels
{
    public class SubjectModel
    {
        [Required(ErrorMessage = "Subject name required!")]
        public string Name { get; set; }
        public List<string> ExistingSubjectNames { get; set; }
    }
}
