﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace NIS_project.ViewModels
{
    public class FileModel
    {
        public List<IFormFile> Files { get; set; }
        public int Id { get; set; }
    }
}
